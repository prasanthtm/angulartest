import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from '../pages/card/card.component';
import { CarouselModule } from 'ngx-owl-carousel-o';


@NgModule({
  declarations: [
    CardComponent
  ],
  imports: [
    CommonModule,
    CarouselModule
  ],
  exports: [
    CardComponent,
    CarouselModule
  ]

})
export class SharedModule { }
