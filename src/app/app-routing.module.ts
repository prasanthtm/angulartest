import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [ /* {
  path: '**',
  redirectTo: 'home',
},  */
  {
    path: 'home',
    loadChildren: async () => (await import('./pages/home/home.module')).HomeModule
  },
  //Artist url
   {
    path: 'artist/:id',
    loadChildren: async () => (await import('./pages/artists/artists.module')).ArtistsModule
  }  
  /*  { path: 'artist', loadChildren: () => ArtistsModule } */
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
