import { Component, HostBinding } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-home-container',
  templateUrl: './home-container.component.html',
  styleUrls: ['./home-container.component.scss']
})
 
export class HomeContainerComponent  {
 
  smartlistner: any = [];
  recentListner:any = [];

  constructor(private http: HttpClient) {

  }

  ngOnInit(){
    //Smart Listner JSON array
    this.http.get<any>("./assets/smart.json").subscribe((data)=>
      this.smartlistner = data
    )
    //Recent Listner JSON array
    this.http.get<any>("./assets/recent.json").subscribe((data)=>
      this.recentListner = data
    )
  }



  
  @HostBinding('class.container-fluid')
  @HostBinding('class.d-block')
  readonly hostClasses = true;

}
