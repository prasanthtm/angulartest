import { NgModule } from '@angular/core';
import { HomeContainerComponent } from './containers/home-container/home-container.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    HomeContainerComponent
  
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([{ path: '', component: HomeContainerComponent }])
  ]

})
export class HomeModule {
}
