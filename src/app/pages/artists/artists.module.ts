import { RouterModule } from '@angular/router';
import { ArtistContainerComponent } from './artist-container/artist-container.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [
    ArtistContainerComponent
  ],
  imports: [

    SharedModule,
    RouterModule.forChild([{ path: '', component: ArtistContainerComponent }])
  ]
})
export class ArtistsModule { }
