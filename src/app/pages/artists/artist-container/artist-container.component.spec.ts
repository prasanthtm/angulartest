import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ArtistContainerComponent } from './artist-container.component';
import { SharedModule } from 'src/app/shared/shared.module';

describe('ArtistContainerComponent', () => {
  let component: ArtistContainerComponent;
  let fixture: ComponentFixture<ArtistContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArtistContainerComponent ],
      imports: [HttpClientTestingModule, RouterTestingModule,SharedModule]

    })
    .compileComponents();

    fixture = TestBed.createComponent(ArtistContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
