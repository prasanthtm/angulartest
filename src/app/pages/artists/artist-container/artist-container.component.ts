import { Component, HostBinding, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from "@angular/common/http";


@Component({
  selector: 'app-artist-container',
  templateUrl: './artist-container.component.html',
  styleUrls: ['./artist-container.component.scss']
})
export class ArtistContainerComponent implements OnInit {
  
  id : any
  smartlistner: any = [];
  recentListner:any = [];


  constructor( private route:ActivatedRoute , private http: HttpClient) { }

  ngOnInit(): void  {
    this.id = this.route.snapshot.paramMap.get("id");
     //Smart Listner JSON array
     this.http.get<any>("./assets/smart.json").subscribe((data)=>
     this.smartlistner = data
   )
   //Recent Listner JSON array
   this.http.get<any>("./assets/recent.json").subscribe((data)=>
     this.recentListner = data
   )
  }


}
